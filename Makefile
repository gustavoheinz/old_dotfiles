# TODO Organize dots inside a folder, so that it's easier to make a makefile
DOTS = beets git gpg mpd mpv mutt nethack ranger scripts tmux vim wm zathura zsh

all: $(DOTS)
	echo "Done"

$(DOTS):
	echo "Stowing $@..."
	stow -R $@

beets: beets_config_dir
	echo "Stowing $@..."
	stow -R $@

gpg: gnupg_home_dir
	chmod 700 $(HOME)/.gnupg
	echo "Stowing $@..."
	stow -R $@

mpd: mpd_config_dir
	echo "Stowing $@..."
	stow -R $@

mpv: mpv_config_dir
	echo "Stowing $@..."
	stow -R $@

mutt: mutt_home_dir
	echo "Stowing $@..."
	stow -R $@

ranger: ranger_config_dir
	echo "Stowing $@..."
	stow -R $@

tmux: tmux_home_dir
	echo "Stowing $@..."
	stow -R $@

vim: vim_home_dir nvim_config_dir
	echo "Stowing $@..."
	stow -R $@

zathura: zathura_config_dir
	echo "Stowing $@..."
	stow -R $@

wm: bspwm_config_dir sxhkd_config_dir polybar_config_dir
	echo "Stowing $@..."
	stow -R $@

%_home_dir:
	@if [[ ! -d "$(HOME)/.$*" ]]; then \
		mkdir $(HOME)/.$*; \
	fi

%_config_dir:
	@if [[ ! -d "$(XDG_CONFIG_HOME)/$*" ]]; then \
		mkdir $(XDG_CONFIG_HOME)/$*; \
	fi
