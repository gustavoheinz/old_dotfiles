#!/bin/sh

set -e

SYSDRIVERS=/sys/bus/pci/drivers
SYSEHCI=$SYSDRIVERS/ehci-pci
SYSUHCI=$SYSDRIVERS/uhci_hcd


if [ "$(id -u)" != 0 ]; then
    echo "This script should run as root"
    exit 1
fi

if [ -d $SYSUHCI ]; then
    cd $SYSUHCI && \
        for i in ????:??:??.?; do
            printf "%s \n" "Restarting $i"
            printf %s "$i" > unbind
            printf %s "$i" > bind
        done
fi

if [ -d $SYSEHCI ]; then
    cd $SYSEHCI && \
        for i in ????:??:??.?; do
            printf "%s \n" "Restarting $i"
            printf %s "$i" > unbind
            printf %s "$i" > bind
        done
fi
