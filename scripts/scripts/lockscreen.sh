#! /bin/sh

# Script to pause mpd, mute volume, blank the screen and lock the screen.

# Pause mpd
mpc pause

# Mute ALSA
amixer -q sset Master 0%

i3lock -e -c '000000'
