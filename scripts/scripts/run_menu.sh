#!/bin/sh

set -e

source "$HOME/scripts/colors.sh"

#TODO font variable
dmenu_run -q -i -h 25 -p "run:" \
    -nb "$COLOR_BACKGROUND" \
    -sb "$COLOR_BACKGROUND_ALT" \
    -sf "$COLOR_FOREGROUND"
