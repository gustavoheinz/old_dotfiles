#! /bin/sh

BASE16_THEME_FILE=$HOME/.base16_theme
# TODO A better command
eval $(cat ~/.base16_theme | grep Base | grep -v "_" | cut -d "#" -f 1 | sed -e 's/\///g' -e 's/="/="#/g')

# TODO Better variables
export COLOR_FOREGROUND=$color07
export COLOR_FOREGROUND_ALT=$color08
export COLOR_BACKGROUND=$color00
export COLOR_BACKGROUND_ALT=$color08
export COLOR_ALERT=$color01
