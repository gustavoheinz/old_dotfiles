local utils = require 'mp.utils'
function subdl_load()
    subdl = "/usr/bin/subdl"
    mp.msg.info("Searching subtitle...")
    mp.osd_message("Searching subtitle...")
    t = {}
    t.args = {subdl, "--lang=pob", "--download=best-rating", "--existing=overwrite", mp.get_property("path")}
    res = utils.subprocess(t)
    if res.status == 0 then
        mp.commandv("rescan_external_files", "reselect")
        mp.msg.info("Subtitle download succeeded")
        mp.osd_message("Subtitle download succeeded")
    else
        mp.msg.warn("Subtitle download failed")
        mp.osd_message("Subtitle download failed")
    end
end

mp.add_key_binding("b", "subdl_load_subs", subdl_load)
