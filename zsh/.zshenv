# Important env variables
export EDITOR="nvim"
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR
export XDG_CONFIG_HOME="$HOME/.config"
export LC_ALL="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

# While qutebrowser does not add webengine by default
alias qutebrowser="$(which qutebrowser) --backend=webengine"

# Python Virtualenvs
export WORKON_HOME=~/.virtualenvs

# Add ~/scripts to PATH
export PATH="$PATH:$HOME/scripts"

# PATH on OSX
if [[ "$(uname)" == "Darwin" ]]; then
    # coreutils from brew
    export PATH="usr/local/opt/coreutils/libexec/gnubin:$PATH"
    # homebrew
    export PATH="usr/local/bin:/usr/local/sbin:$PATH"
fi
