# Bind keys for zsh
source ~/.zsh/binds.zsh

# History options for zsh
source ~/.zsh/history.zsh

# Setopts for zsh
source ~/.zsh/setopt.zsh

# Exports for dev env
source ~/.zsh/exports.zsh

# Aliases
source ~/.zsh/aliases.zsh

# Plugins
source ~/.zsh/plugins.zsh

# Completion configs
source ~/.zsh/completion.zsh

# Extra configs for shell
source ~/.zsh/extras.zsh
