alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias 'bk=cd $OLDPWD'

alias vim='nvim'
alias vi='nvim'

alias tmux='tmux -u'
alias ta='tmux attach -t'
alias tnew='tmux new -s'
alias tls='tmux ls'
alias tkill='tmux kill-session -t'

alias ev='vim ~/.config/nvim/init.vim'
alias et='vim ~/.tmux.conf'
alias ez='vim ~/.zshrc'

if [[ "$(uname)" == "Darwin" ]]; then
    alias ls='ls -Gh'
else
    alias ls='ls --color=auto -h'
fi
