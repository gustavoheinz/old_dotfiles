zstyle ':zplug:tag' depth 0

# Check if zplug is installed
if [[ ! -d ~/.zplug ]]; then
    curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
fi

# Load zplug
source ~/.zplug/init.zsh


# Plugins
zplug "mafredri/zsh-async"

zplug "zsh-users/zsh-completions"
zplug "tj/git-extras", as:plugin, use:"etc/*.zsh"

zplug "clvv/fasd", as:command, hook-build:"make"
if command -v fasd >/dev/null 2>&1; then
  eval "$(fasd --init zsh-hook zsh-ccomp zsh-ccomp-install \
      zsh-wcomp zsh-wcomp-install posix-alias)"
fi

zplug "sapegin/shipit", as:command, use:bin/shipit
zplug "unixorn/git-extra-commands"

zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-syntax-highlighting", defer:2

zplug "chriskempson/base16-shell", as:plugin, use:base16-shell.plugin.zsh

zplug "sindresorhus/pure"

if ! zplug check; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

zplug load

# FZF
if [[ ! "$PATH" == *$HOME/.fzf/bin* ]]; then
  export PATH="$PATH:$HOME/.fzf/bin"
fi
[[ $- == *i* ]] && source "$HOME/.fzf/shell/completion.zsh" 2> /dev/null
source "$HOME/.fzf/shell/key-bindings.zsh"
