# Add rust binaries to path
if [[ -d "$HOME/.cargo" ]]; then
    export PATH="$HOME/.cargo/bin:$PATH"
fi

# Tmux should have a different TERM for 256-color output
if [[ ! -z "$TMUX" ]]; then
    export TERM=screen-256color
else
    export TERM=xterm-256color
fi

# I use a different HD for vagrant stuff
if [[ -d '/media' ]]; then
    export VAGRANT_HOME="/media/vagrantd"
fi

# Default ledger file
if type ledger > /dev/null; then
    export LEDGER_FILE=$HOME/.ledger/current.ldg
fi

# FZF should show hidden files
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'

# Golang variables
export GOPATH=~/.go
