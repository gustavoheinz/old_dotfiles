if [[ "$(uname)" == "Darwin" ]]; then
    source /usr/local/bin/virtualenvwrapper_lazy.sh
    export SSH_AUTH_SOCK=$HOME/.gnupg/S.gpg-agent.ssh
else
    source /usr/bin/virtualenvwrapper_lazy.sh
    # Set GPG TTY
    export GPG_TTY=$(tty)

    # Refresh gpg-agent tty in case user switches into an X session
    gpg-connect-agent updatestartuptty /bye >/dev/null
fi
