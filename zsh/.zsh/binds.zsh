# History Substring Keys
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind keys to Emacs mode
bindkey -e
