set encoding=utf-8
set nocompatible

filetype off

" Vundle Auto-Install
if (!isdirectory(expand("$HOME/.vim/bundle/Vundle.vim")))
    call system(expand("mkdir -p $HOME/.vim/bundle"))
    call system(expand("git clone https://github.com/gmarik/vundle $HOME/.vim/bundle/Vundle.vim"))
    echo 'Vundle was freshly installed. You should run :PluginInstall on vim'
endif

" Vundle Settings
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'bling/vim-airline'
Plugin 'chriskempson/base16-vim'
Plugin 'tpope/vim-fugitive'
Plugin 'altercation/vim-colors-solarized'
Plugin 'edkolev/tmuxline.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'scrooloose/syntastic'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'kien/ctrlp.vim'
Plugin 'easymotion/vim-easymotion'

cal vundle#end()

" Vim Settings
syntax on
set softtabstop=4
set tabstop=4
set shiftwidth=4
set expandtab
set colorcolumn=80
set laststatus=2
set ttimeoutlen=50
set number
set backspace=indent,eol,start

" NERDTree Settings
map <silent> <C-n> :NERDTreeFocus<CR>

" CtrlP Settings
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" CtrlP Funky Settings
nnoremap <Leader>fu :CtrlPFunky<Cr>
"" narrow the list down with a word under cursor
nnoremap <Leader>fU :execute 'CtrlPFunky ' . expand('<cword>')<Cr>

" Easy Motion Settings
let g:EasyMotion_do_mapping = 0
nmap <Space> <Plug>(easymotion-s2)
let g:EasyMotion_smartcase = 1

" Copy/Paste/Cut
noremap YY "+y<CR>
noremap P "+gP<CR>
noremap XX "+x<CR>

" Syntastic Settings
let g:syntastic_python_python_exec = '/usr/bin/python2.7'
let g:syntastic_python_checkers=['flake8']

" YouCompleteMe Settings
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

" Color
set background=dark
colorscheme base16-tomorrow

" VimAirline Settings
let g:airline_powerline_fonts = 1
let g:airline_theme='base16'
let g:tmuxline_preset = {
    \'a'    : '#S',
    \'win'  : '#I #W',
    \'cwin' : '#I #W',
    \'y'    : '%R',
    \'z'    : '#h',
	\'options' : {
	\'status-justify' : 'left'}
	\}

