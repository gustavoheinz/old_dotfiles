set encoding=utf-8

" Plug Auto-Install
if empty(glob("$HOME/.config/nvim/autoload/plug.vim"))
    call system(expand("curl -fLo $HOME/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"))
    echo 'VimPlug was freshly installed. You should run :PlugInstall on vim'
endif

" Plug Settings
call plug#begin('~/.config/nvim/plugged')

" Essential Plugins
Plug 'tpope/vim-obsession'

" Cosmetic Plugins
Plug 'mhinz/vim-startify'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'weihanglo/tmuxline.vim'
Plug 'chriskempson/base16-vim'

" Git Related Plugins
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

Plug 'bronson/vim-trailing-whitespace'
Plug 'Yggdroot/indentLine'
Plug 'vim-scripts/Align'

" Syntax Plugins
Plug 'fntlnz/atags.vim'
Plug 'sheerun/vim-polyglot'
Plug 'ledger/vim-ledger'
Plug 'w0rp/ale'

" Productivity plugins
Plug 'tpope/vim-commentary'
Plug 'terryma/vim-expand-region'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-dispatch'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'

" Completion Plugins
function! DoRemote(arg)
  UpdateRemotePlugins
endfunction
Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
Plug 'zchee/deoplete-jedi', { 'for': 'python' }
Plug 'zchee/deoplete-go', { 'for': 'go', 'do': 'make' }

" Snippets plugins
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Misc plugins
Plug 'vimwiki/vimwiki'

call plug#end()

" Vim Settings
set softtabstop=4
set tabstop=4
set shiftwidth=4
set expandtab
set colorcolumn=80
set laststatus=2
set number
set inccommand=split
set clipboard=unnamed

" Concealing
set conceallevel=0
let g:tex_conceal = ""

" Make markdown be the default filetype
autocmd BufEnter * if &filetype == "" | setlocal ft=markdown | endif

" Useful binds
let mapleader = "\<Space>"
nnoremap <Leader>q :q<CR>
nnoremap <silent> <F5> :setlocal spell! spelllang=pt<CR>
nnoremap Q <Nop>

" Vim Expand
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" Color settings
set termguicolors
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif
" Stop vim from drawing background colors, since they follow the same
" colorscheme of the terminal
hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE

" Dispatch settings
nnoremap <F6> :Start! ./ship<CR>

" Startify Settings
let g:startify_session_dir = '~/.config/nvim/session'
let g:startify_custom_header = startify#fortune#cowsay()
let g:startify_bookmarks = [ '~/.config/nvim/init.vim',
                           \ '~/.zshrc',
                           \ '~/.tmux.conf']
let g:startify_change_to_dir = 0
let g:startify_change_to_vcs_root = 1

" FZF Settings
nmap <leader>ff :Files<CR>
nmap <leader>fg :Ag<space>

" Fugitive Settings
let g:gitgutter_highlight_lines = 1
nmap <leader>gs :Gstatus<CR>
nmap <leader>gc :Gcommit<CR>
nmap <leader>gp :Gpush<CR>
nmap <leader>gr :Gread<CR>
nmap <leader>gw :Gw<CR>

" GitGutter Settings
nmap <Leader>ha <Plug>GitGutterStageHunk
nmap <Leader>hu <Plug>GitGutterUndoHunk
nmap <Leader>hv <Plug>GitGutterPreviewHunk

" Easy Motion Settings
let g:EasyMotion_smartcase = 1
map  <Leader>ee <Plug>(easymotion-bd-f)
nmap <Leader>ee <Plug>(easymotion-overwin-f)
map  <Leader>ew <Plug>(easymotion-bd-w)
nmap <Leader>ew <Plug>(easymotion-overwin-w)
map  <Leader>el <Plug>(easymotion-bd-jk)
nmap <Leader>el <Plug>(easymotion-overwin-line)

" Atags Settings
map <Leader>t :call atags#generate()<cr>
let g:atags_build_commands_list = [
    \"touch tags.tmp",
    \"ctags -R *.py -f tags.tmp",
    \"awk 'length($0) < 400' tags.tmp > tags",
    \"rm -f tags.tmp"
    \]

" Ale Settings
let g:ale_lint_on_text_changed = 'never'
let g:ale_statusline_format = ['E %d', 'W %d', 'OK']
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {
\   'python': ['pylint'],
\}

" Deoplete Settings
let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#jedi#show_docstring = 1
autocmd CompleteDone * pclose
let g:deoplete_ignore_sources = ['buffer']
let g:python_host_prog = $HOME."/.virtualenvs/neovim/bin/python"
let g:python3_host_prog = $HOME."/.virtualenvs/neovim3/bin/python"

" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsEditSplit="vertical"

" VimWiki
let g:vimwiki_list = [{
  \ 'path': '$HOME/vimwiki',
  \ 'template_path': '$HOME/vimwiki/templates',
  \ 'template_default': 'default',
  \ 'template_ext': '.html'}]

" VimAirline Settings
let g:airline_powerline_fonts = 1
let g:airline_theme='base16'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#branch#displayed_head_limit = 15

function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))

    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors

    return l:counts.total == 0 ? 'OK' : printf(
    \   '%dW %dE',
    \   all_non_errors,
    \   all_errors
    \)
endfunction

let g:airline_section_error = '%{LinterStatus()}'

" Tmuxline Settings
let g:tmuxline_preset = {
\'a'    : '#S',
\'win'  : '#I #W',
\'cwin' : '#I #W',
\'y'    : '%R',
\'z'    : '#h',
\'options' : {
    \'status-justify' : 'left'}
    \}
